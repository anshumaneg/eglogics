﻿using System;
using System.Collections.Generic;
using Eglogics.ViewModel;
using Xamarin.Forms;

namespace Eglogics.View
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
            BindingContext = new LoginPageViewModel();
        }
    }
}
