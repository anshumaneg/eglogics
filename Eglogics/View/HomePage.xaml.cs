﻿using System;
using System.Collections.Generic;
using Eglogics.ViewModel;
using Xamarin.Forms;

namespace Eglogics.View
{
    public partial class HomePage : ContentPage
    {
        public HomePage()
        {
            InitializeComponent();
            BindingContext = new HomePageViewModel();
        }
    }
}
